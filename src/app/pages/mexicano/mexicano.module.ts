import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MexicanoPageRoutingModule } from './mexicano-routing.module';

import { MexicanoPage } from './mexicano.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MexicanoPageRoutingModule
  ],
  declarations: [MexicanoPage]
})
export class MexicanoPageModule {}
