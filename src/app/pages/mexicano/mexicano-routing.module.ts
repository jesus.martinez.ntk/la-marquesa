import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MexicanoPage } from './mexicano.page';

const routes: Routes = [
  {
    path: '',
    component: MexicanoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MexicanoPageRoutingModule {}
