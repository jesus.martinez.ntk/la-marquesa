import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';

import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-mexicano',
  templateUrl: './mexicano.page.html',
  styleUrls: ['./mexicano.page.scss'],
})
export class MexicanoPage implements OnInit {
  entrada: any;
  guisados1: any;
  guisados2: any;
  postre: any;
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  constructor(
    private alertController: AlertController,
    private http: HTTP,
    private storage: NativeStorage,
    private utils:UtilsService
  ) { }

  ngOnInit() {
    this.storage.getItem('evento').then((response)=>{
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
      this.entrada = response.menu['8'].Entrada ? response.menu['8'].Entrada : '';
      this.guisados1 = response.menu['8']['Guisados sin Carne'] ? response.menu['8']['Guisados sin Carne'] : '';
      this.guisados2 = response.menu['8']['Guisados con Carne'] ? response.menu['8']['Guisados con Carne'] : '';
      this.postre = response.menu['8'].Postre ? response.menu['8'].Postre : '';
    }).catch((err)=>{
      console.log(err);
    });
  }
  async inputentrada(texto,variable){
    let vl = '';
    switch(variable){
      case 'entrada':
        vl = this.entrada;
        break;
      case 'guisados1':
        vl = this.guisados1;
        break;
      case 'guisados2':
        vl = this.guisados2;
        break;
      case 'postre':
        vl = this.postre;
        break;
    }

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Agregar '+texto,
      inputs: [
        {
          name: 'valor',
          type: 'textarea',
          placeholder: '',
          value: vl
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (alertData) => {
            let cat = '';

            switch(cat){
              case 'entrada':
                cat = 'Entrada';
                break;
              case 'guisados1':
                cat = 'Guisados sin Carne';
                break;
              case 'guisados2':
                cat = 'Guisados con Carne';
                break;
              case 'postre':
                cat = 'Postre';
                break;
            }
            const url = 'categoria=' + cat + '&val=' + alertData.valor + '&idEvento=' + this.id + '&menu=8';

            this.http.get(environment.api + 'SaveMenu.php?' + url, {}, {}).then((response) => {
              this.utils.toastPresentOk("Menu Guardado");
              switch(variable){
                case 'entrada':
                  this.entrada = alertData.valor;
                  break;
                case 'guisados1':
                  this.guisados1 = alertData.valor;
                  break;
                case 'guisados2':
                  this.guisados2 = alertData.valor;
                  break;
                case 'postre':
                  this.postre = alertData.valor;
                  break;
              }
            }).catch((err)=>{
              this.utils.toastPresentError(err+" Error al Guardar el Menu");
            });
          }
        }
      ]
    });

    await alert.present();

}
}
