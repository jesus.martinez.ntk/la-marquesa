import { Component, OnInit } from '@angular/core';
import { AngularDelegate } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-internacional',
  templateUrl: './internacional.page.html',
  styleUrls: ['./internacional.page.scss'],
})
export class InternacionalPage implements OnInit {
  ensalada: any;
  pasta: any;
  guarnicion: any;
  fuerte: any;
  postre: any;
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  constructor(
    private alertController: AlertController,
    private storage: NativeStorage,
    private http: HTTP,
    private utils:UtilsService
    ) { }

  ngOnInit() {

    this.storage.getItem('evento').then((response)=>{
      console.log(response);
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
      this.guarnicion = response.menu['7'].Guarnicion ? response.menu['7'].Guarnicion : '';
      this.fuerte = response.menu['7']['Plato Fuerte'] ? response.menu['7']['Plato Fuerte'] : '';
      this.postre = response.menu['7'].Postre ? response.menu['7'].Postre : '';
      this.ensalada = response.menu['7'].Ensaladas ? response.menu['7'].Ensaladas : '';
      this.pasta = response.menu['7'].Pasta ? response.menu['7'].Pasta : '';
    }).catch((err)=>{
      console.log(err);
    });

  }
  async inputentrada(texto,variable){
    let vl = '';
    switch(variable){
      case 'ensalada':
        vl = this.ensalada;
        break;
      case 'pasta':
        vl = this.pasta;
        break;
      case 'guarnicion':
        vl = this.guarnicion;
        break;
      case 'fuerte':
          vl = this.fuerte;
          break;
      case 'postre':
        vl = this.postre;
        break;
    }

    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Agregar '+texto,
      inputs: [
        {
          name: 'valor',
          type: 'textarea',
          placeholder: '',
          value: vl
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');

          }
        }, {
          text: 'Ok',
          handler: (alertData) => {

            let cat = '';
            switch(variable){
              case 'ensalada':
                cat = 'Ensaladas';
                break;
              case 'pasta':
                cat = 'Pasta';
                break;
              case 'guarnicion':
                cat = 'Guarnicion';
                break;
              case 'fuerte':
                cat = 'Plato Fuerte';
                break;
              case 'postre':
                cat = 'Postre';
                break;
            }

            const url = 'categoria=' + cat + '&val=' + alertData.valor + '&idEvento=' + this.id + '&menu=7';
            this.http.get(environment.api + 'SaveMenu.php?' + url, {}, {}).then((response) => {

              switch(variable){
                case 'ensalada':
                  this.ensalada = alertData.valor;
                  break;
                case 'pasta':
                  this.pasta = alertData.valor;
                  break;
                case 'guarnicion':
                  this.guarnicion = alertData.valor;
                  break;
                case 'fuerte':
                  this.fuerte = alertData.valor;
                  break;
                case 'postre':
                  this.postre = alertData.valor;
                  break;
              }
              this.utils.toastPresentOk("Menu Guardado");

            }).catch((err)=>{
              this.utils.toastPresentError(err+" Error al Guardar el Menu");
            });

          }
        }
      ]
    });

    await alert.present();

}
}
