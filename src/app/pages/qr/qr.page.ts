import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { UtilsService } from '../../services/utils/utils.service';


@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit {
  lugares =[];
  errorimg1=0;
  errorimg2=0;
  lugarid=0;
  id: any;
  hrefant1:any;
  hrefant2:any;
  src1:any;
  src2:any;
  showb1=true;
  showb2=true;
  base64Image='';
  fecha = '';
  altaupdate = "alta";
  data={
    categoria:'',
    nombreCliente:''
  };
  ports = [];
  port= {
    id:'',
    name:'',
    ubicacion:''
  };
  tipoqr = '1';
  disponible = false;
  exist:any;
  constructor(
    private camera: Camera,
    private http: HTTP,
    private route: ActivatedRoute,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.data.categoria = params.categoria;
      this.data.nombreCliente = params.nombreCliente;
      this.id = params.id;
      this.lugarid = params.lugar;
      this.fecha = params.fecha;
      this.existeqr();
    });
    
  }
  addimage(){
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      cameraDirection: 1,
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL
    };
  
      this.camera.getPicture(options).then((imagendata) => {
       // imageData is either a base64 encoded string or a file URI
       // If it's base64 (DATA_URL):
        this.disponible = true;
       //console.log(ima);
        this.base64Image = 'data:image/jpeg;base64,' + imagendata;
        //this.giardarimg(imagendata);
      
       //this.createimge(base64Image);
  
      }, (err) => {
       // Handle error
      });
    
  }
 
 
  giardarimg(){
    console.log({menu:this.tipoqr,fecha:this.fecha});
    if(this.tipoqr === "1"){
      if(this.showb1){
        this.altaupdate = "update";
      } else {
        this.altaupdate = "alta";
      }
    }else{
      if(this.showb2){
        this.altaupdate = "update";
      } else {
        this.altaupdate = "alta";
      }
    }
    console.log(this.altaupdate);
    this.utils.presentLoader();
    this.http.post(environment.api + 'sendqr.php',{menu:this.tipoqr,imgdata:this.base64Image,fecha:this.fecha, alta:this.altaupdate},{}).then(response => {
      console.log(response);
      let info = JSON.parse(response.data);
      if(info.Codigo == "200"){
        this.utils.toastPresentOk(info.Mensaje);

      } else {
        this.utils.toastPresentError(info.Mensaje);

      }
      window.caches.delete;
      this.utils.dismissLoader();
    }).catch((err) => {
      this.utils.toastPresentError(err+ "Error al enviar Imagen");
      this.utils.dismissLoader();
    });
  }
  backbutton(){
    this.utils.backbutton();
  }
  existeqr() {
    this.utils.presentLoader();
    let tr = this.fecha.split("-");
  
    let nfecha = tr[1] + "/" + tr[0] + "/" +tr[2];
    this.hrefant1 = 'http://108.175.11.174:8095/api/EventoMobile?id_tipo_menu=1&fecha_evento='+nfecha;
    this.hrefant2 = 'http://108.175.11.174:8095/api/EventoMobile?id_tipo_menu=2&fecha_evento='+nfecha;
    
    this.src1='http://108.175.11.174:8095/Menus/Evento1_'+parseInt(tr[1])+parseInt(tr[0])+tr[2]+'120000AM.jpeg';
    this.src2='http://108.175.11.174:8095/Menus/Evento2_'+parseInt(tr[1])+parseInt(tr[0])+tr[2]+'120000AM.jpeg';
    console.log(this.src1);
    console.log(this.src2);
  }
  errorImage1(){
    this.errorimg1 = 1;
    this.showb1 = false;
  }
  errorImage2(){
    this.errorimg2 = 1;
    this.showb2 = false;
  }
}
