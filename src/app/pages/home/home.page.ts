import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(private utils: UtilsService) { }

  ngOnInit() {
  }
  navigate(page){
    this.utils.navigate(page);
  }
}
