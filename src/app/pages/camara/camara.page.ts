import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment.prod';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { UtilsService } from '../../services/utils/utils.service';


@Component({
  selector: 'app-camara',
  templateUrl: './camara.page.html',
  styleUrls: ['./camara.page.scss'],
})
export class CamaraPage implements OnInit {
  data={
    categoria:'',
    nombreCliente:''
  };
  id:any;
  imgsrc=[];
  dta:any;
  slideOpts = {
    initialSlide: 0,
    speed: 800,
    autoplay:true
  };
  constructor(
    private camera: Camera,
    private http: HTTP,
    private route:ActivatedRoute,
    private actionSheetController: ActionSheetController,
    private utils:UtilsService
    ) { }

  ngOnInit() {
    
    this.route.queryParams.subscribe(params => {
      this.data.categoria = params.categoria;
      this.data.nombreCliente = params.nombreCliente;
      this.id = params.id;
      this.http.post(environment.api + 'GetImg.php?id='+this.id,{},{}).then(response => {
        console.log(response);
        let data = JSON.parse(response.data);
        this.imgsrc = data.Images.map(result =>{
          return {source:environment.api+'show.php?id='+  result.id,id:result.id};
        });
      }).catch((err)=>{
        this.utils.toastPresentError(err+" Error al cargar las Imagenes");

      });
    });
   
  }
  abrirgaleria(){
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      cameraDirection: 1,
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,

      
      
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      const base64Image = imageData;
      this.updateimg(base64Image);
     }, (err) => {
      // Handle error
      this.utils.toastPresentError(err+" Error al subir Imagen");

     });
  }
  abrircamara(){
    const options: CameraOptions = {
      saveToPhotoAlbum: false,
      correctOrientation: true,
      cameraDirection: 1,
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth:500
    };

    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     const base64Image = imageData;
     this.updateimg(base64Image);

    }, (err) => {
     // Handle error
     this.utils.toastPresentError(err+" Error al subir Imagen");

    });
  }
  updateimg(base){
    console.log(base);
    this.http.post(environment.api + 'SaveImg.php',{id:this.id,imgdata:base},{}).then(response => {
      console.log(JSON.parse(response.data));
      if(JSON.parse(response.data)){
      this.imgsrc.push({source:environment.api+'show.php?id='+JSON.parse(response.data),id:JSON.parse(response.data)});
      this.utils.toastPresentOk("Se ha Guardado la Imagen");
      } else {
        this.utils.toastPresentError("Error al Guardar la Imagen");
      }
    }).catch(err => {
      this.utils.toastPresentError(err);

    });
  }
  async seleccionar(){
    const actionSheet = await this.actionSheetController.create({
      header: "Seleccionar Origen de Imagen",
      buttons: [{
              text: 'Cargar desde Libreria',
              handler: () => {
                  this.abrirgaleria();
              }
          },
          {
              text: 'Usar Camara',
              handler: () => {
                  this.abrircamara();
              }
          },
          {
              text: 'Cancel',
              role: 'cancel'
          }
      ]
  });
  await actionSheet.present();
  }
  borrarimg(id, indice){
    this.http.post(environment.api + 'DelImg.php',{id:id},{}).then(response => {
      let dt = parseInt(JSON.parse(response.data));
      if(dt){
        this.utils.toastPresent('Imagen Borrada');
         this.imgsrc.splice(indice, 1);
        console.log(this.imgsrc);
      }else{
        this.utils.toastPresent('Error al Borrar imagen');

      }
    }).catch(err => {
      this.utils.toastPresent('Error al Borrar imagen');
    });
  }
  backbutton(){
    this.utils.backbutton();
  }
}
