import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment.prod';
import { ActivatedRoute } from '@angular/router';
import { UtilsService } from '../../services/utils/utils.service';


@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {
  data={
    categoria:'',
    nombreCliente:''
  };
  imagenesant=[];
  imgsrc=[];
  constructor(
    private http: HTTP,
    private utils: UtilsService
    ) { }
    slideOpts = {
      initialSlide: 0,
      pager: true
    };
    slideOpts2 = {
      initialSlide: 0,
      speed: 800
    };
  ngOnInit() {
    this.http.post(environment.api + 'GetImgs.php',{},{}).then(response => {
      console.log(response);
      let data = JSON.parse(response.data);
      console.log(data);
      for(let item in data['Images']){
        this.imgsrc.push({
          "data":data['Images'][item].map(result =>{
          return environment.api+'show.php?id='+  result;
        }),
        "fecha":item,
        click: false
      });
      }
      console.log(this.imgsrc);
      this.cargarsig();
    });
    
  }
  backbutton(){
    this.utils.backbutton();
  }
  cargarsig(){
    this.http.post(environment.api + 'PathFotos.php',{},{}).then(response => {
      let data = JSON.parse(response.data);
      this.imagenesant=data.PathFotos;
      console.log(this.imagenesant);
      for(let i in this.imagenesant){
        let n = 1;
        for(let j in this.imgsrc){
          if(this.imagenesant[i].computed+"||"+this.imagenesant[i].categoria+'||'+this.imagenesant[i].id_Evento == this.imgsrc[j].fecha){
            this.imgsrc[j].data.push("http://ecem.ddns.net:8099/marquesa/lamarquesa/my/Imagenes/Fotos_Eventos/"+this.imagenesant[i].pathFoto);
            n=0;
            break;
          }
        }
        if(n){
          this.imgsrc.push({
            "data":["http://ecem.ddns.net:8099/marquesa/lamarquesa/my/Imagenes/Fotos_Eventos/"+this.imagenesant[i].pathFoto],
            "fecha":this.imagenesant[i].computed+"||"+decodeURIComponent(escape(this.imagenesant[i].categoria))+'||'+this.imagenesant[i].id_Evento,
            click:false
          });
        }
       
      }
      console.log(this.imgsrc);
    });
  }
  toggleSection(i) {
    this.imgsrc[i].click = !this.imgsrc[i].click;
  }
}
