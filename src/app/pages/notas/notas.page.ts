import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-notas',
  templateUrl: './notas.page.html',
  styleUrls: ['./notas.page.scss'],
})
export class NotasPage implements OnInit {
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  notas: any;
  constructor(
    private http: HTTP,
    private storage: NativeStorage,
    private utils:UtilsService

  ) { }

  ngOnInit() {
    this.storage.getItem('evento').then((response)=>{
      console.log(response);
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
      this.notas = response.notas;
    }).catch((err)=>{
      console.log(err);
    });
  }
  changeval(){
    this.http.get(environment.api + 'SaveNota.php?idEvento=' + this.id + '&notas=' + this.notas, {}, {}).then((response) => {
      this.utils.toastPresentOk("Nota Guardada");
    }).catch((err)=>{
      this.utils.toastPresentError(err+" Error al Guardar la Nota");

    });
  }
  backbutton(){
    this.utils.backbutton();
  }
}
