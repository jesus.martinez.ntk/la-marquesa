import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'tiempos',
        children:[
          {
            path: '',
            loadChildren: () => import('../tiempos/tiempos.module').then( m => m.TiemposPageModule )
          }
        ]
      },
      {
        path: 'internacional',
        children:[
          {
            path: '',
            loadChildren: () => import('../internacional/internacional.module').then( m => m.InternacionalPageModule )
          }
        ]
      },
      {
        path: 'mexicano',
        children:[
          {
            path: '',
            loadChildren: () => import('../mexicano/mexicano.module').then( m => m.MexicanoPageModule )
          }
        ]
      },
      {
        path: 'brindis',
        children:[
          {
            path: '',
            loadChildren: () => import('../brindis/brindis.module').then( m => m.BrindisPageModule )
          }
        ]
      },

      {
        path: '',
        redirectTo: '/menu/tiempos',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/menu/tiempos',
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPageRoutingModule {}
