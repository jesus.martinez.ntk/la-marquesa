import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  idevento: any;
  constructor(
    private utils: UtilsService,
    private storage: NativeStorage,
    private route: ActivatedRoute


  ) { }

  ngOnInit() {
    this.storage.getItem('evento').then((response)=>{
      console.log(response);
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
    }).catch((err)=>{
      console.log(err);
    });
  }
  backbutton(){
    this.utils.backbutton();
  }
}
