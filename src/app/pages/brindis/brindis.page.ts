import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { AlertController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-brindis',
  templateUrl: './brindis.page.html',
  styleUrls: ['./brindis.page.scss'],
})
export class BrindisPage implements OnInit {
  brindis: any;
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  constructor(private utils: UtilsService,
    private alertController: AlertController,
    private http: HTTP,
    private storage: NativeStorage
    ) { }

  ngOnInit() {
    this.brindis = '';

    this.storage.getItem('evento').then((response)=>{
      console.log(response);
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
      this.brindis = response.menu['12'].Brindis ? response.menu['12'].Brindis : '';
    }).catch((err)=>{
      console.log(err);
    });
  }
  async inputentrada(texto,variable){

      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Agregar '+texto,
        inputs: [
          {
            name: 'valor',
            type: 'text',
            placeholder: '',
            value: this.brindis
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');

            }
          }, {
            text: 'Ok',
            handler: (alertData) => {
              if(variable === 'brindis'){
                this.brindis = alertData.valor;
              }
              const url = 'categoria=Brindis&val=' + alertData.valor + '&idEvento=' + this.id + '&menu=12';
              this.http.get(environment.api + 'SaveMenu.php?' + url, {}, {}).then((response) => {
                  this.brindis = alertData.valor;
                  this.utils.toastPresentOk("Guardado Exitoso");
              }).catch((err)=>{
                this.utils.toastPresentError(err);
              });
            }
          }
        ]
      });

      await alert.present();

  }
}
