import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrindisPageRoutingModule } from './brindis-routing.module';

import { BrindisPage } from './brindis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrindisPageRoutingModule
  ],
  declarations: [BrindisPage]
})
export class BrindisPageModule {}
