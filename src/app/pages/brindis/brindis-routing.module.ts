import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrindisPage } from './brindis.page';

const routes: Routes = [
  {
    path: '',
    component: BrindisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrindisPageRoutingModule {}
