import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TiemposPageRoutingModule } from './tiempos-routing.module';

import { TiemposPage } from './tiempos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TiemposPageRoutingModule
  ],
  declarations: [TiemposPage]
})
export class TiemposPageModule {}
