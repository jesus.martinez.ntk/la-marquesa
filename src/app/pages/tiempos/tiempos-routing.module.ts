import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TiemposPage } from './tiempos.page';

const routes: Routes = [
  {
    path: '',
    component: TiemposPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TiemposPageRoutingModule {}
