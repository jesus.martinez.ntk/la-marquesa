import { HTTP } from '@ionic-native/http/ngx';
import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-tiempos',
  templateUrl: './tiempos.page.html',
  styleUrls: ['./tiempos.page.scss'],
})
export class TiemposPage implements OnInit {
  entrada: any;
  fuerte: any;
  crema: any;
  postre: any;
  data={
    categoria:'',
    nombreCliente:''
  };
  id: any;
  constructor(
    private http: HTTP,
    private utils: UtilsService,
    private alertController: AlertController,
    private route: ActivatedRoute,
    private storage: NativeStorage,

    ) { }

  ngOnInit() {
    this.entrada = '';
    this.crema = '';
    this.fuerte = '';
    this.postre = '';

    this.storage.getItem('evento').then((response)=>{
      console.log(response);
      this.data.categoria = response.categoria;
      this.data.nombreCliente = response.nombreCliente;
      this.id = response.id;
      this.crema = response.menu['6'].Crema ? response.menu['6'].Crema : '';
      this.fuerte = response.menu['6']['Plato Fuerte'] ? response.menu['6']['Plato Fuerte'] : '';
      this.postre = response.menu['6'].Postre ? response.menu['6'].Postre : '';
      this.entrada = response.menu['6'].Entrada ? response.menu['6'].Entrada : '';
    }).catch((err)=>{
      console.log(err);
    });
  }
  async inputentrada(texto, variable){
    let vl = '';
    console.log(variable);
    switch(variable){
      case 'entrada':
        vl = this.entrada;
        break;
      case 'crema':
        vl = this.crema;
        break;
      case 'fuerte':
        vl = this.fuerte;
        break;
      case 'postre':
        vl = this.postre;
        break;
    }

    console.log("vl",vl);
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Agregar '+texto,
        inputs: [
          {
            name: 'valor',
            type: 'textarea',
            placeholder: '',
            value: vl
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');

            }
          }, {
            text: 'Ok',
            handler: (alertData) => {


              let cat = '';
              switch(variable){
                case 'entrada':
                  cat = 'Entrada';
                  break;
                case 'crema':
                  cat = 'Crema';
                  break;
                case 'fuerte':
                  cat = 'Plato Fuerte';
                  break;
                case 'postre':
                  cat = 'Postre';
                  break;
              }
              console.log("car",cat);

              const url = 'categoria=' + cat + '&val=' + alertData.valor + '&idEvento=' + this.id + '&menu=6';
              console.log(url);
              this.http.get(environment.api + 'SaveMenu.php?' + url, {}, {})
              .then((response) => {
                this.utils.toastPresentOk("Menu Guardado");

                switch(variable){
                  case 'entrada':
                    this.entrada = alertData.valor;
                    break;
                  case 'crema':
                    this.crema = alertData.valor;
                    break;
                  case 'fuerte':
                    this.fuerte = alertData.valor;
                    break;
                  case 'postre':
                    this.postre = alertData.valor;
                    break;
                }
              }).catch((err)=>{
                this.utils.toastPresentError(err+" Error al Guardar el Menu");

              });



            }
          }
        ]
      });

      await alert.present();

  }
}
