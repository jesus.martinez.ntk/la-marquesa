import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  data={
    categoria: '',
    nombreCliente: '',
    fecha: ''
  };
  id: any;
  dta: any;
  constructor(
    private http: HTTP,
    private utils: UtilsService,
    private route: ActivatedRoute,
    private storage: NativeStorage
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params.id;
      this.http.get(environment.api + 'Evento.php?var=' + this.id, {}, {}).then((data)=>{
        this.dta = JSON.parse(data.data).DatosMenu[0];
        this.data.categoria = this.dta.categoria;
        this.data.nombreCliente = this.dta.nombreCliente;
        this.data.fecha = this.dta.fecha;
        this.storage.setItem('evento', this.dta);

      }).catch((error)=>{
        console.log(error);
      });
    });
  }

  navigate(page){
    this.utils.navigate(page,{
      id: this.id,
      nombreCliente:this.data.nombreCliente,
      categoria:this.data.categoria,
      lugar: this.dta.idLugar,
      fecha: this.data.fecha
    });
  }
  backbutton(){
    this.utils.navigater("home");
  }
}
