import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar';
import { UtilsService } from '../../services/utils/utils.service';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { environment } from '../../../environments/environment';
declare var moment;

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.page.html',
  styleUrls: ['./calendario.page.scss'],
})
export class CalendarioPage implements OnInit {
  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  eventSource =[];
  data: any;
  eventos: any;
  viewTitle: string;
  mesanio=[];
  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
  mes: any;
  anio: any;
  dia: any;
  dataeventos: any;
  selectedDate: Date;

  constructor(
    private http: HTTP,
    private utils: UtilsService,
    private storage: NativeStorage
    ) { }
  

  ngOnInit() {

   this.mesanio = [];
  }
  backbutton(){
    this.utils.backbutton();
  }
  back(){
    this.myCal.slidePrev();
  }
  next(){
    this.myCal.slideNext();
  }
  onViewTitleChanged(title){
    this.viewTitle = title;
  }
  navigate(page){
    const asd = {
      fecha: this.anio+'-'+this.mes+'-'+this.dia
    };
    this.utils.navigate(page, asd);
  }

  onEventSelected = (event) => {
    console.log('evento',event);
    this.utils.navigate('dashboard', {id:event.id});

  };
  eventSelected(event){
    console.log('evento',event);
    this.utils.navigate('dashboard', {id:event.id});

  }

  inserevent(){

  }

  onCurrentChanged = (ev: Date) => {
    this.mes = ev.getMonth()+1;
    this.anio = ev.getFullYear();
    this.dia = ev.getDate();
    this.geteventos();
  };

  geteventos(){
    if(typeof this.mesanio[this.mes+''+this.anio] === 'undefined') {
      this.http.get(environment.api + 'EventosXMes.php?var=' + this.mes + '&var2=' + this.anio,{},{}).then((data)=>{
        this.dataeventos = JSON.parse(data.data);
        console.log("ok");
        const fec = this.anio+'-'+this.mes;
        this.eventos = this.dataeventos.DatosMenu;
        for(const intem in this.eventos){
          let txt = this.eventos[intem].categoria;
          let deta = this.eventos[intem].nombreCliente;
          let fc = new Date(this.eventos[intem].fecha);
          console.log();
          let startTimeMomentObj = new Date(moment(this.eventos[intem].fecha).toDate());
          let endTimeMomentObj = new Date(moment(this.eventos[intem].fecha).toDate());
          const event = {
            id: this.eventos[intem].id,
            title: txt,
            startTime: startTimeMomentObj,
            endTime: endTimeMomentObj,
            idCliente:this.eventos[intem].idCliente,
            adultos:this.eventos[intem].adultos,
            categoria:this.eventos[intem].categoria,
            fecha:this.eventos[intem].fecha,
            nombreCliente:this.eventos[intem].nombreCliente,
            detalles:deta
          };
          this.eventSource.push(event);
        }
        this.myCal.loadEvents();
        console.log(this.eventSource);
        this.mesanio[this.mes+''+this.anio] = true;
    });
    }
  }
}
