import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils/utils.service';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastController, LoadingController, Platform, NavController } from '@ionic/angular';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.page.html',
  styleUrls: ['./evento.page.scss'],
})
export class EventoPage implements OnInit {
fecha: string;
tinvitados = 0;
adultos: number;
ninos: number;
ports: any;
port: any;
telefono: any;
categoria='';
datos=[];
  constructor(
    private storage: NativeStorage,
    private route: ActivatedRoute,
    private utils: UtilsService,
    private http: HTTP,
    public navController: NavController
    ) {

  }
  backbutton(){
    this.utils.backbutton();
  }
  filterPorts(text: string) {
    return this.datos.filter(port => {
      return port.name.toLowerCase().indexOf(text) !== -1 ||
        port.id.toString().toLowerCase().indexOf(text) !== -1;
    });
  }

  searchPorts(event: {
    component: IonicSelectableComponent,
    text: string
  }) {
    let text = event.text.trim().toLowerCase();
    event.component.startSearch();

    console.log(text);

    if (!text || text.length <= 2) {
      // Close any running subscription.
     

      event.component.items = [];
      event.component.endSearch();
      return;
    }
    console.log(this.filterPorts(text));
    event.component.items = this.filterPorts(text);
    event.component.endSearch();
  }
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.fecha = params.fecha;
    });

    this.tinvitados = 0;
    this.adultos = 0;
    this.ninos = 0;
    this.utils.presentLoader();
    this.http.get( environment.api + 'ListaClientes.php',{},{}).then((data)=>{
      const dta = JSON.parse(data.data);
      console.log(dta.ListaClientes);
      this.ports = [];//dta.ListaClientes;
      this.datos = dta.ListaClientes;
      this.utils.dismissLoader();
    }).catch((err)=>{
      this.utils.toastPresentError(err+" Error al cargar Clientes");
      this.utils.dismissLoader();
    });
  }

  setnewcliente(){
    this.utils.presentAlertPrompt().then((dat)=>{
      console.log(dat.valor);
      if(dat.valor!=='')
      {this.http.get(environment.api + 'InsertCliente.php?var=' + dat.valor ,{},{}).then((data)=>{
        const dta = JSON.parse(data.data);
        console.log(dta);
        const prt = {
          id:dta.id,
          name: dat.valor,
          tel:''
        };
        this.ports.push(prt);
        this.port = prt;
        this.utils.toastPresentOk("Se ha Guardado el Cliente");

      }).catch((err)=>{
        this.utils.toastPresentError(err+" Error al Insertar Cliente");

      });}
    }).catch(()=>{

    });
  }
  portChange(valor){
    console.log(valor);
    console.log(this.port);
    this.telefono = this.port.tel;
  }
  personas(){
    this.adultos = Math.round(this.adultos);
    this.ninos = Math.round(this.ninos);
    this.tinvitados = this.adultos + this.ninos;
  }
  guardar(){
    if(this.tinvitados>0){
      if(this.categoria!==''){
        if(this.port.id>0){
          const url = 'var=' + this.port.id + '&var2=' + this.telefono
          + '&var3=' + this.fecha + '&var4=' + this.adultos + '&var5=' + this.ninos + '&var6=' + this.categoria;
          this.http.get( environment.api + 'InsertEvento.php?' +url,{},{}).then((data)=>{
          const dat = JSON.parse(data.data);
          this.utils.toastPresentOk("Evento Generado");

          if(dat.id){
            this.utils.presentLoader();
            this.navController.pop().then(()=>{
              this.utils.navigate('dashboard',{id: dat.id});
              
            });
          }
          }).catch((err)=>{
            this.utils.toastPresentError(err+" Error al generar evento");

          });
        }else{
            // error cliente
            this.utils.toastPresentError("Cliente vacio");

        }
      }else{
        //error categoria 0
        this.utils.toastPresentError("Categoria vacia");

      }
    } else{
      //error invitados 0
      this.utils.toastPresentError("Invitados vacios");
    }
  }
}
