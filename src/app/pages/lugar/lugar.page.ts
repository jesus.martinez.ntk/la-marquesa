import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-lugar',
  templateUrl: './lugar.page.html',
  styleUrls: ['./lugar.page.scss'],
})
export class LugarPage implements OnInit {
  lugares =[];
  lugarid=0;
  id: any;
  data={
    categoria:'',
    nombreCliente:''
  };
  ports = [];
  port= {
    id:'',
    name:'',
    ubicacion:''
  };
  constructor(
    private http: HTTP,
    private route: ActivatedRoute,
    private storage: NativeStorage,
    private alertController: AlertController,
    private utils:UtilsService
  

  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.data.categoria = params.categoria;
      this.data.nombreCliente = params.nombreCliente;
      this.id = params.id;
      this.lugarid = params.lugar;
      this.http.get( environment.api + 'Lugar.php', {}, {}).then((response)=>{
        const dt = JSON.parse(response.data).Lugar;
        for(const d in dt){
          this.ports.push({id:dt[d].id,name:dt[d].nombre,ubicacion:dt[d].direccion});
          if(this.lugarid == dt[d].id){
            this.port = {id:dt[d].id,name:dt[d].nombre,ubicacion:dt[d].direccion};
          }
        }
        this.utils.toastPresentOk("Lugares Cargados");

      }).catch((error)=>{
        console.log(error);
        this.utils.toastPresentError(error+" Error al cargar lugares");

      });
    });

  }


  portChange(valor){
    console.log(valor);
    console.log(this.port);

  }

  async setnewlugar(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Nuevo Lugar',
      inputs: [
        {
          name: 'nombre',
          type: 'text',
          placeholder: 'Nombre'
        },
        {
          name: 'direccion',
          type: 'textarea',
          placeholder: 'Dirección'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (alertData: any) => {
            this.agregarlugar(alertData);
          }
        }
      ]
    });

    await alert.present();
  }
  agregarlugar(data){
    console.log(data);
    this.http.get(environment.api + 'AddLugar.php?nombre=' + data.nombre + '&direccion=' + data.direccion , {}, {}).then((response)=>{
      const dta = JSON.parse(response.data);
        console.log(dta);
        const prt = {
          id:dta.id,
          name: data.nombre,
          ubicacion: data.direccion
        };
        this.ports.push(prt);
        this.port = prt;
        this.utils.toastPresentOk("Lugar Agregado a la lista");

    }).catch((error)=>{
        console.log(error);
        this.utils.toastPresentError(error+" Error al guardar lugar");

    });
  }
  lugarsave(){
    this.http.get(environment.api + 'SaveLugar.php?id=' + this.id + '&lugarid=' + this.port.id , {}, {}).then((response)=>{
      if(JSON.parse(response.data)){
        this.lugarid = parseInt(this.port.id);
        this.utils.toastPresentOk("Lugar de Evento Guardado");

      } else {

      }
    }).catch((error)=>{
        console.log(error);
        this.utils.toastPresentError(error+" Error al Guardar Lugar");

    });
  }
  backbutton(){
    this.utils.backbutton();
  }
}
