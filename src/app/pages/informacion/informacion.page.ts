import { Component, OnInit } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { UtilsService } from '../../services/utils/utils.service';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {
  categoria = '';
  cliente = '';
  telefono = '';
  fecha = '';
  adultos = 0;
  ninos = 0;
  menus = [];
  dta: any;
  data = {
    categoria:'',
    nombreCliente:''
  };
  id: any;
  lugar = {
    nombre: '',
    direccion: ''
  };
  constructor(
    private http: HTTP,
    private storage: NativeStorage,
    private route: ActivatedRoute,
    private utils: UtilsService

  ) { }
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params.id;
      this.http.get(environment.api + 'Evento.php?var=' + this.id, {}, {}).then((data) => {
        this.dta = JSON.parse(data.data).DatosMenu[0];
        this.data.nombreCliente = this.dta.nombreCliente;
        this.data.categoria = this.dta.categoria;
        this.storage.setItem('evento', this.dta);
        this.data.nombreCliente = this.dta.nombreCliente;
        this.cliente = this.dta.nombreCliente;
        this.categoria = this.dta.categoria;
        this.fecha = this.dta.fecha;
        this.telefono = this.dta.telefono;
        this.lugar.nombre = this.dta.nombreLugar;
        this.lugar.direccion = this.dta.direccionLugar;
        this.ninos = parseInt(this.dta.ninos, 10) ? parseInt(this.dta.ninos, 10) : 0;
        this.adultos = parseInt(this.dta.adultos, 10) ? parseInt(this.dta.adultos, 10) : 0;
        for(const i in this.dta.menu){
          const vr = [];
            for(const j in this.dta.menu[i]){
                vr.push({tipo:j,
                nombre:this.dta.menu[i][j]});
            }
            switch(i){
              case '6':
                this.menus.push({ nombre:'Por Tiempos', menu: vr });
                break;
              case '7':
                this.menus.push({ nombre:'Buffet Internacional',  menu: vr });
                break;
              case '8':
                this.menus.push({ nombre:'Buffet Mexicano',  menu: vr });
                break;
              case '12':
                this.menus.push({ nombre:'Brindis', menu: vr });
                break;
            }
        }
      }).catch((error)=>{
        console.log(error);
      });
    });
  }
  backbutton(){
    this.utils.backbutton();
  }
}
