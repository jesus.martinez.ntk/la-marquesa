import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { UtilsService } from './services/utils/utils.service';
import { AuthenticationService } from './services/authentication/authentication.service';




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private utils: UtilsService,
    private authService: AuthenticationService
) {
    if(this.platform.is('cordova')){
      this.authService.authState.subscribe(async state => {
        state ? this.utils.navigater('inicio') : this.utils.navigater('home');
      });
    } else {
      //this.utils.navigater('dashboard');
    }
  }
}
