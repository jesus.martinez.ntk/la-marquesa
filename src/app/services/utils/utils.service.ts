import { Injectable } from '@angular/core';

import { AlertController } from '@ionic/angular';

import { Router, NavigationExtras } from '@angular/router';
import { ToastController, LoadingController, Platform, NavController } from '@ionic/angular';

import { MenuController } from '@ionic/angular';

import { HTTP } from '@ionic-native/http/ngx';
import { Location } from "@angular/common";



@Injectable({
  providedIn: 'root'
})
export class UtilsService {


  private loading: any;
  constructor(
    private http: HTTP,
    private router: Router,
    private platform: Platform,
    private menu: MenuController,
    public navCtrl: NavController,
    private toastController: ToastController,
    private loadingController: LoadingController,
    public alertController: AlertController,
    private location: Location
  ) { }
  backbutton(){
    this.location.back();
  }
  async toastPresentOk(msg) {
    const toast = await this.toastController.create({
      header: 'Mensaje',
      message: msg,
      position: 'bottom',
      duration: 4000,
      color: 'success'
    });
    await toast.present();

    
  }
  async toastPresentError(msg) {
    const toast = await this.toastController.create({
      header: 'Mensaje',
      message: msg,
      position: 'bottom',
      duration: 4000,
      color: 'danger'
    });
    await toast.present();

    
  }
  
  async toastPresent(msg: string, headerStr?: string) {
    const toast = await this.toastController.create({
      header: headerStr ? headerStr : 'Notificación',
      message: msg,
      position: 'bottom',
      duration: 4000,
      buttons: [
        {
          text: 'Cerrar',
          role: 'cancel'
        }
      ]
    });
    toast.present();
  }
  async dismissLoader() {
    await this.loading.dismiss();
  }
  async presentLoader() {
    this.loading = await this.loadingController.create({
      message: 'Cargando',
      mode: 'ios',
      spinner: 'dots',
    });
    await this.loading.present();
    setTimeout(async ()=>{
      await this.loading.dismiss();
    },1000)
  }

  navigate(route: string, obj1?) {

    const navigationExtras: NavigationExtras = {
      queryParams: obj1
    };

    return this.navCtrl.navigateForward([route], navigationExtras);
  }
  navigater(route: string, obj1?, optionalData?, obj2?) {

    const navigationExtras: NavigationExtras = {
      queryParams: {
        body: JSON.stringify(obj1),
        optional: optionalData,
        obj: JSON.stringify(obj2),
      }
    };

    // return this.router.navigate([route], navigationExtras);
    return this.navCtrl.navigateRoot(route);
  }

  async openmenu(menu) {
    this.menu.open(menu);
  }
  async closemenu() {
    this.menu.close('first');
    this.menu.close('sesion');
  }
  async presentAlertPrompt(): Promise<any> {
    return new Promise<any>(async (resolve, reject) => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Nuevo Cliente',
        inputs: [
          {
            name: 'valor',
            type: 'text',
            placeholder: 'Nombre'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
              reject();
            }
          }, {
            text: 'Ok',
            handler: (alertData: any) => {
              resolve(alertData);
            }
          }
        ]
      });

      await alert.present();
    });
  }
}
